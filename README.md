hangman
=======

Første mappeinnlevering i AppUtvikling ved HiOA (Android)


Oppgavetekst: 

Mappe 1 høst 2014
Dere skal lage et Hangman-spill. Ordene som skal gjettes skal ligge i arrays.xml. Det skal være registrert minst 15 ord. Når applikasjonen startes skal brukeren kunne velge om han vil se regler, starte spill, endre språk eller avslutte applikasjonen.
Når spillet starter må man kunne registrere bokstaver. Riktige bokstaver settes på plass i ordet, mens gale bokstaver vises et annet sted på skjermen. For hvert gale svar blir endres galgebildet til man er hengt.
Det skal registreres og vises hvor mange spill man har vunnet og hvor mange man har tapt. Når et ord er benyttet en gang skal det ikke benyttes mer i denne spilleøkten.
Spillet skal tilpasses norsk og engelsk. Endres språk til engelsk skal all tekst i applikasjonen endres til engelsk.
Det er et pluss hvis man implementerer ny layout når orientering av skjermen endres.
Spillet vil bli kjørt på en emulator Nexus S (4.0,480 X 800:hdpi) Android 4.4.2 (API 19)
Det skal leveres dokumentasjon av programmet og en tegning som viser hvilke klasser som er laget og hvordan disse vekselvirker. Gå på developer.android.com og les om design-prinsipper. Begrunn hvordan du har benyttet disse prisnsippene i ditt prosjekt.
Prosjektet skal navngis med studentnummer <studentnr>. Dokumentasjonen skal ligge i prosjektmappen.
Det legges vekt på:
Design 30%
Struktur kode 20% Funksjonalitet 30% Dokumentasjon 20%
Start gjerne tidlig. Begynn å tenk på hvordan det skal se ut og hvordan man best navigerer i applikasjonen. Kom gjerne på lab for tips og tilbakemeldinger før du leverer.
Innleveringsfrist er fredag 16.00 i uke 38. Tidsfristen er absolutt og legeerklæring må leveres hvis den ikke overholdes. Dette er en karaktergivende mappe som teller 30% av sluttkarakter i kurset.
