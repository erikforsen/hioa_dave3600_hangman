package no.forsen.hangman;

import no.forsen.hangmann.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AboutActivity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState ){
		super.onCreate(savedInstanceState);
		Language.loadLang(this); 
		setContentView(R.layout.activity_about);
		TextView t = (TextView)findViewById(R.id.txt_postit);
		t.setTypeface(MainActivity.tf); 	
	}
    
	@Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
	@Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.quit_application:
    		finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
} // end of class AboutActivity