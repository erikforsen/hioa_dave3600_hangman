package no.forsen.hangman;

import no.forsen.hangmann.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class NewGameDialog extends DialogFragment {
	private DialogClickListener callback;
	private final static String TITLE = "title";
	private final static String MESSAGE = "message";
	
	public interface DialogClickListener{
		public void onYesClick();
		public void onNoClick(); 
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState ){
		super.onCreate(savedInstanceState);
		try{
			callback = (DialogClickListener) getActivity();
		}catch (ClassCastException e){
			throw new ClassCastException(getString(R.string.classexception));
		}
	}
	
	public static NewGameDialog newInstance(int title,String message){
		NewGameDialog frag = new NewGameDialog();
		Bundle args = new Bundle();
		args.putInt(TITLE,title);
		args.putString(MESSAGE, message);
		frag.setArguments(args);
 
		return frag; 
	}
	
	public Dialog onCreateDialog(Bundle savedInstanceState){
		int title = getArguments().getInt(TITLE);
		String message = getArguments().getString(MESSAGE);
		setCancelable(false);
		return new AlertDialog.Builder(getActivity())
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(R.string.yes,
				new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int whichButton){
				callback.onYesClick();
			}
		}
		)
		.setNegativeButton(R.string.no,
				new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int whichButton){
				callback.onNoClick();
			}
		}
		)
		.create();
	}
} // end of class NewGameDialog