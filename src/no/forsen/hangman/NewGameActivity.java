package no.forsen.hangman;

import no.forsen.hangman.NewGameDialog.DialogClickListener;
import no.forsen.hangmann.R;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class NewGameActivity extends Activity implements DialogClickListener {
	private final static String SAVE_GAME = "savegame";
	private final static String SAVE_CORRECT = "savecorrect"; 
	private final static String SAVE_WRONG = "savewrong";
	private final static String SAVE_CLICKS = "saveclicks"; 

	private int letters; 
	private GameLogic game; 
	private ImageView[] dash;
	private int pixels; 
	private ImageView[] hanging;
	private RelativeLayout myLayout; 
	private RelativeLayout.LayoutParams hangparams; 
	private int[] btn_state_crct;
	private int[] btn_state_wrng; 
	private int btn_clicks = 0; 

	@Override
	public void onYesClick(){
        Intent newGame = new Intent();
        newGame.putExtra(MainActivity.GAME_LOGIC,game);
        newGame.putExtra(MainActivity.NEW_GAME, true);
        setResult(RESULT_OK,newGame);
        finish();
	}
	@Override 
	public void onNoClick(){
    	Intent gameData = new Intent();
    	gameData.putExtra(MainActivity.GAME_LOGIC, game);
    	gameData.putExtra(MainActivity.NEW_GAME, false);
    	setResult(RESULT_OK,gameData);
        finish();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(SAVE_GAME, game);
		outState.putIntArray(SAVE_CORRECT, btn_state_crct);
		outState.putIntArray(SAVE_WRONG, btn_state_wrng);
		outState.putInt(SAVE_CLICKS, btn_clicks); 
		
	}
	@Override
	protected void onCreate( Bundle savedInstanceState){
		super.onCreate(savedInstanceState); 
		Language.loadLang(this); 
		setContentView(R.layout.activity_newgame); 
		myLayout = (RelativeLayout)findViewById(R.id.activity_newgame_layout);
		final float scale = getBaseContext().getResources().getDisplayMetrics().density;
		pixels = (int) (12 * scale + 0.5f);
		Intent intent = getIntent();
		
		hanging = new ImageView[7];
		for(int i = 0; i<hanging.length; i++)
			hanging[i] = new ImageView(this); 
			
		hanging[0].setImageResource(R.drawable.hang_0); 
		hanging[1].setImageResource(R.drawable.hang_1);
		hanging[2].setImageResource(R.drawable.hang_2);
		hanging[3].setImageResource(R.drawable.hang_3);
		hanging[4].setImageResource(R.drawable.hang_4);
		hanging[5].setImageResource(R.drawable.hang_5);
		hanging[6].setImageResource(R.drawable.hang_6); 
		hangparams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,RelativeLayout.LayoutParams.WRAP_CONTENT);
		hangparams.setMargins(20,20,0,0); 
		
		
		myLayout.addView(hanging[0],hangparams);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT); 
		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
			params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			params.setMargins(20, 0, 0, 20);
		}
		else{
			params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE); 
			params.setMargins(0, 0, 20, 20);
		}
		params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);	
		
		
		
		if(savedInstanceState != null){
			game = (GameLogic) savedInstanceState.getParcelable(SAVE_GAME);
			btn_state_crct = savedInstanceState.getIntArray(SAVE_CORRECT);
			btn_state_wrng = savedInstanceState.getIntArray(SAVE_WRONG); 
			btn_clicks = savedInstanceState.getInt(SAVE_CLICKS); 
		}
		else{
			game = (GameLogic) intent.getParcelableExtra(MainActivity.GAME_LOGIC);
			if(!game.newGame())
			{
				game = new GameLogic(getResources().getStringArray(R.array.words));
				game.newGame();
				NewGameDialog dialog = NewGameDialog.newInstance(R.string.finish, getString(R.string.finish_message));
				dialog.show(getFragmentManager(), "newdialog");

			}
			btn_state_crct = new int[29];
			btn_state_wrng = new int[29];
			
		}		
		
		letters = game.getWordLength(); 
		dash = new ImageView[letters]; 


		if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
			dash[0] = new ImageView(this); 
			dash[0].setImageResource(R.drawable.dash);
			dash[0].setId(1); 		
			myLayout.addView(dash[0], params );
			for (int i = 1; i < letters; i++ ) {
				dash[i] = new ImageView(this); 
				dash[i].setImageResource(R.drawable.dash);
				dash[i].setId(i+1);
				
				RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
				params1.addRule(RelativeLayout.RIGHT_OF, dash[i-1].getId());
				params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE); 
				params1.setMargins(10, 0, 0, 20);
				myLayout.addView(dash[i], params1);	
			}
		}
		else
		{
			dash[letters-1] = new ImageView(this); 
			dash[letters-1].setImageResource(R.drawable.dash);
			dash[letters-1].setId(letters); 
			myLayout.addView(dash[letters-1],params); 
			for (int i = letters - 2; i >= 0; i--){
				dash[i] = new ImageView(this);
				dash[i].setImageResource(R.drawable.dash);
				dash[i].setId(i+1); 
				
				RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
				params1.addRule(RelativeLayout.LEFT_OF, dash[i+1].getId());
				params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
				params1.setMargins(0,0,10,20);
				myLayout.addView(dash[i], params1);
			}
		}
		
		
		OnClickListener ocl = new View.OnClickListener(){
			public void onClick(View v){
				playChar(v);
			}
		};
		
		final Button[] buttons = new Button[29]; 
		
		buttons[0] = (Button)findViewById(R.id.btn_a);
		buttons[1] = (Button)findViewById(R.id.btn_b);
		buttons[2] = (Button)findViewById(R.id.btn_c);
		buttons[3] = (Button)findViewById(R.id.btn_d);
		buttons[4] = (Button)findViewById(R.id.btn_e);
		buttons[5] = (Button)findViewById(R.id.btn_f);
		buttons[6] = (Button)findViewById(R.id.btn_g);
		buttons[7] = (Button)findViewById(R.id.btn_h);
		buttons[8] = (Button)findViewById(R.id.btn_i);
		buttons[9] = (Button)findViewById(R.id.btn_j);
		buttons[10] = (Button)findViewById(R.id.btn_k);
		buttons[11] = (Button)findViewById(R.id.btn_l);
		buttons[12] = (Button)findViewById(R.id.btn_m);
		buttons[13] = (Button)findViewById(R.id.btn_n);
		buttons[14] = (Button)findViewById(R.id.btn_o);
		buttons[15] = (Button)findViewById(R.id.btn_p);
		buttons[16] = (Button)findViewById(R.id.btn_q);
		buttons[17] = (Button)findViewById(R.id.btn_r);
		buttons[18] = (Button)findViewById(R.id.btn_s);
		buttons[19]= (Button)findViewById(R.id.btn_t);
		buttons[20] = (Button)findViewById(R.id.btn_u);
		buttons[21] = (Button)findViewById(R.id.btn_v);
		buttons[22] = (Button)findViewById(R.id.btn_w);
		buttons[23] = (Button)findViewById(R.id.btn_x);
		buttons[24] = (Button)findViewById(R.id.btn_y);
		buttons[25] = (Button)findViewById(R.id.btn_z);
		buttons[26] = (Button)findViewById(R.id.btn_ae);
		buttons[27] = (Button)findViewById(R.id.btn_oe);
		buttons[28] = (Button)findViewById(R.id.btn_aa);
		
		for(Button b : buttons )
		{
			b.setTypeface(MainActivity.tf);
			b.setOnClickListener(ocl);
		}
		
		if(!getResources().getConfiguration().locale.getLanguage().equalsIgnoreCase("no") && !getResources().getConfiguration().locale.getLanguage().equalsIgnoreCase("nb"))
		{
			buttons[26].setVisibility(View.GONE);
			buttons[27].setVisibility(View.GONE);
			buttons[28].setVisibility(View.GONE);
		}
		
		if(savedInstanceState != null){
			SparseArray<Object> played = game.getPlayed();
			for( int i = 0; i < played.size(); i++){
				int key = played.keyAt(i);
				updateWord(dash, key, (Character) played.get(key));
			}
			
			for(int i = 1; i <= game.getTries(); i++ )
				myLayout.addView(hanging[i],hangparams);
			
			Button b; 
			
			for(int i = 0; i < btn_clicks; i++){
				if(btn_state_crct[i] != 0){
					b = (Button)findViewById(btn_state_crct[i]); 
					b.setEnabled(false);
					b.setBackgroundResource(R.drawable.correct);
				}
				else if(btn_state_wrng[i] != 0){
					b = (Button)findViewById(btn_state_wrng[i]);
					b.setEnabled(false);
					b.setBackgroundResource(R.drawable.wrong);
				}					
			}
		}
	}
	
	public void updateWord(ImageView[] dash, int index, char c ){
		TextView t = new TextView(getBaseContext()); 
		t.setText(Character.toString(Character.toUpperCase(c))+" "); 
		t.setTextColor(Color.BLACK);
		t.setTypeface(MainActivity.tf,Typeface.BOLD);
		t.setTextSize(pixels);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.ABOVE, dash[index].getId());
		params.addRule(RelativeLayout.ALIGN_RIGHT, dash[index].getId());
		params.setMargins(0, 0, -9, -25);
		myLayout.addView(t,params); 
		letters--; 

	}
	
	public void playChar(View v){
		Button b = (Button)v; 
		Character c = b.getText().charAt(0);
		boolean correct = false; 
		boolean[] result = game.play(Character.toLowerCase(c)); 
		for(int i = 0; i < result.length; i++ ){
			if ( result[i] ){
				updateWord(dash, i, c);
				correct = true;
			}
		}
		
		b.setEnabled(false); 
		int m = (correct ? R.drawable.correct : R.drawable.wrong ); 
		
		if(!correct){
			btn_state_wrng[btn_clicks++] = b.getId(); 
			
			if(game.getTries() == 6)
				myLayout.removeView(hanging[1]); 
			
			myLayout.addView(hanging[game.getTries()],hangparams);
		}
		else
			btn_state_crct[btn_clicks++] = b.getId(); 
		
		b.setBackgroundResource(m); 
		
		if(letters == 0){
			NewGameDialog dialog = NewGameDialog.newInstance(R.string.new_game, game.gameOver(true, getBaseContext()));
			dialog.show(getFragmentManager(), "dialog");
		}
		
		if(game.getTries()==6){
			NewGameDialog dialog = NewGameDialog.newInstance(R.string.new_game, game.gameOver(false, getBaseContext()));
			dialog.show(getFragmentManager(), "dialog");
		}
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
	@Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.quit_application:
    		finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
} // end of class NewGameActivity
