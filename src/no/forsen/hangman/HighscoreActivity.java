package no.forsen.hangman;

import no.forsen.hangmann.R;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class HighscoreActivity extends Activity implements OnClickListener{
	private TextView highscore_global_win;
	private TextView highscore_global_loose;
	private TextView highscore_local_win;
	private TextView highscore_local_loose;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Language.loadLang(this);
		setContentView(R.layout.activity_highscore);
		intent = getIntent();
		
		TextView total = (TextView)findViewById(R.id.txt_total);
		TextView total_vic = (TextView)findViewById(R.id.txt_total_vic);
		TextView total_defeat = (TextView)findViewById(R.id.txt_total_defeat);
		
		TextView session = (TextView)findViewById(R.id.txt_session);
		TextView session_vic = (TextView)findViewById(R.id.txt_session_vic);
		TextView session_defeat = (TextView)findViewById(R.id.txt_session_defeat); 
		
		total.setTypeface(MainActivity.tf);
		total_vic.setTypeface(MainActivity.tf);
		total_defeat.setTypeface(MainActivity.tf);
		
		session.setTypeface(MainActivity.tf);
		session_vic.setTypeface(MainActivity.tf);
		session_defeat.setTypeface(MainActivity.tf); 
		
		highscore_global_win = (TextView)findViewById(R.id.highscore_global_win);
		highscore_global_loose = (TextView)findViewById(R.id.highscore_global_loose);
		highscore_local_win = (TextView)findViewById(R.id.highscore_local_win);
		highscore_local_loose = (TextView)findViewById(R.id.highscore_local_loose);
		
		highscore_global_win.setTypeface(MainActivity.tf);
		highscore_global_loose.setTypeface(MainActivity.tf);
		highscore_local_win.setTypeface(MainActivity.tf);
		highscore_local_loose.setTypeface(MainActivity.tf); 
		
		highscore_global_win.setText(Integer.toString(intent.getIntExtra(MainActivity.GLOBAL_HIGHSCORE_WIN,0)));
		highscore_global_loose.setText(Integer.toString(intent.getIntExtra(MainActivity.GLOBAL_HIGHSCORE_LOOSE, 0)));
		highscore_local_win.setText(Integer.toString(intent.getIntExtra(MainActivity.LOCAL_HIGHSCORE_WIN, 0)));
		highscore_local_loose.setText(Integer.toString(intent.getIntExtra(MainActivity.LOCAL_HIGHSCORE_LOOSE,0)));	
		
		Button btn_reset = (Button)findViewById(R.id.btn_reset);
		btn_reset.setTypeface(MainActivity.tf,Typeface.BOLD);
		btn_reset.setOnClickListener(this); 
		
	}

	@Override
	public void onClick(View v) {

		highscore_global_win.setText(Integer.toString(0));
		highscore_global_loose.setText(Integer.toString(0));
		highscore_local_win.setText(Integer.toString(0));
		highscore_local_loose.setText(Integer.toString(0));	

		setResult(RESULT_OK);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
	@Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.quit_application:
    		finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
} // end of class HighscoreActivity