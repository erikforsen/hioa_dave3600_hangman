package no.forsen.hangman;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import no.forsen.hangmann.R;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

public class GameLogic implements Parcelable {

	private String word;
	private Game current; 
	private int win;
	private int loose; 
	private List<String> words;
	
	public GameLogic(String[] w){
		words = new ArrayList<String>(Arrays.asList(w)); 
		win = 0; 
		loose = 0; 
		Collections.shuffle(words);
	}
	
	public GameLogic(Parcel pc){ 		
		words = new ArrayList<String>();
		current = pc.readParcelable(Game.class.getClassLoader()); 
		pc.readList(words,null);
		win = pc.readInt();
		loose = pc.readInt(); 
	}
	
	public String getWord(){
		return word;
	}
	
	public int getWin() {
		return win;
	}
	
	public void setWin(int win) {
		this.win = win;
	}
	
	public int getLoose() {
		return loose;
	}
	
	public void setLoose(int loose) {
		this.loose = loose;
	}
	
	public SparseArray<Object> getPlayed() {
		return current.getPlayed();
	}
	
	public boolean newGame(){
		if(words.size() == 0)
			return false;
		
		word = words.get(0); 
		words.remove(0); 
		current = new Game(word); 
		return true;

	}
	
	public boolean[] play( char c ){ 
		return current.play(c); 
	}
	
	public int getWordLength(){
		return word.length(); 
	}
	public int getTries(){
		return current.getTries(); 
	}
	
	public String gameOver(Boolean win, Context ct){
		StringBuilder message = new StringBuilder();
		
		if(win){
			setWin(getWin()+1);
			MainActivity.highscore_win++;
			message.append(ct.getString(R.string.you_won));
		}
		else{
			setLoose(getLoose()+1); 
			MainActivity.highscore_loose++;
			message.append(ct.getString(R.string.you_lost));
			message.append(" \"");
			message.append(getWord());
			message.append("\"");
		}
		message.append("\n");
		message.append(ct.getString(R.string.current_win));
		message.append(Integer.toString(getWin()));
		message.append("\n");
		message.append(ct.getString(R.string.current_lost));
		message.append(Integer.toString(getLoose()));
		
		return message.toString(); 
		
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dst, int arg1) {
		dst.writeParcelable(current, arg1); 
		dst.writeList(words);
		dst.writeInt(win);
		dst.writeInt(loose);
	}
	public static final Parcelable.Creator<GameLogic> CREATOR = new Parcelable.Creator<GameLogic>() {
	    public GameLogic createFromParcel(Parcel in) {
	    	return new GameLogic(in);
	    }

	    public GameLogic[] newArray(int size) {
	        return new GameLogic[size];
	    }
	};
} // end of class GameLogic
