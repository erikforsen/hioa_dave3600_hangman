package no.forsen.hangman;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

public class Game implements Parcelable{
	private String word;
	private int tries; 
	private boolean correct; 
	private SparseArray<Object> played; 

	
	public Game(String w){
		played = new SparseArray<Object>(); 
		word = w; 
		tries = 0;
	}

	@SuppressWarnings("unchecked")
	public Game(Parcel pc){
		tries =  pc.readInt(); 
		word = pc.readString(); 
		played =  pc.readSparseArray(null); 

	}
		
	public boolean[] play( char c ){
		correct = false; 
		boolean[] result = new boolean[word.length()]; 
		
		for( int i = 0; i < word.length(); i++ ){
			if(c == word.charAt(i)){
				played.put(i,c );
				result[i] = true; 
				correct = true; 
			}
		}
		
		if(!correct)
			tries++; 
		
		return result; 
	}

	public int getTries(){
		return tries; 
	}

	public SparseArray<Object> getPlayed(){
		return played; 
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dst, int arg1) {
		dst.writeInt(tries); 
		dst.writeString(word);
		dst.writeSparseArray(played);

	}

	public static final Parcelable.Creator<Game> CREATOR = new Parcelable.Creator<Game>() {
	    public Game createFromParcel(Parcel in) {
	    	return new Game(in);
	    }

	    public Game[] newArray(int size) {
	        return new Game[size];
	    }
	};
} // end of class Game
