package no.forsen.hangman;

import no.forsen.hangmann.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {
	final static String LOCAL_HIGHSCORE_WIN = "lclhghwin";
	final static String LOCAL_HIGHSCORE_LOOSE = "lclhghlos";
	final static String GLOBAL_HIGHSCORE_WIN = "gblhghwin";
	final static String GLOBAL_HIGHSCORE_LOOSE = "gblhghlos";
	final static String GAME_LOGIC = "gamelogic";
	final static String NEW_GAME = "newgame";
	final static String SAVE_GAME = "savegame";
	final static int HIGHSCORE_RESULT=444;
	final static int GAME_LOGIC_RESULT = 666; 
	final static int LANGUAGE_RESULT = 555; 
	final static int QUIT = 999; 
	
	private GameLogic game;
	public static int highscore_win; 
	public static int highscore_loose; 
	private SharedPreferences score; 
	public static Typeface tf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Language.loadLang(this);
        tf = Typeface.createFromAsset(getAssets(), "fonts/handwriting.ttf");
        setContentView(R.layout.activity_main);
        game = new GameLogic( getResources().getStringArray(R.array.words));
        score = getSharedPreferences(SAVE_GAME, Context.MODE_PRIVATE);
        
        final Button btn_new = (Button)findViewById(R.id.btn_new);
        final Button btn_high = (Button)findViewById(R.id.btn_hgh_scr);
        final Button btn_instr = (Button)findViewById(R.id.btn_about);
        final Button btn_lang = (Button)findViewById(R.id.btn_lang);
        final Button btn_quit = (Button)findViewById(R.id.btn_quit); 
        
        btn_new.setTypeface(tf,Typeface.BOLD);
        btn_high.setTypeface(tf,Typeface.BOLD);
        btn_instr.setTypeface(tf,Typeface.BOLD);
        btn_lang.setTypeface(tf,Typeface.BOLD);
        btn_quit.setTypeface(tf,Typeface.BOLD);
        
        View.OnClickListener ocl = new View.OnClickListener() {
        	
			@Override
			public void onClick(View v) {
				switch(v.getId()){
				case R.id.btn_new:
					newGame();
					break;
				case R.id.btn_hgh_scr:
					highScore();
					break;
				case R.id.btn_about:
					about();
					break;
				case R.id.btn_lang:
					lang();
					break;
				case R.id.btn_quit:
					finish();
					break;
				default:
					break;
				}	
			}
		};     

        btn_new.setOnClickListener(ocl);
        btn_high.setOnClickListener(ocl);
        btn_instr.setOnClickListener(ocl);
        btn_lang.setOnClickListener(ocl);
        btn_quit.setOnClickListener(ocl);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
        
        SharedPreferences.Editor editor;
       
        if(highscore_win > score.getInt(GLOBAL_HIGHSCORE_WIN, 0)){
        	editor = score.edit();
        	editor.putInt(GLOBAL_HIGHSCORE_WIN, highscore_win); 
        	editor.commit();
        }
        else
        	highscore_win = (score.getInt(GLOBAL_HIGHSCORE_WIN, 0));
        
        
        if(highscore_loose > score.getInt(GLOBAL_HIGHSCORE_LOOSE, 0)){
        	editor = score.edit();
        	editor.putInt(GLOBAL_HIGHSCORE_LOOSE, highscore_loose);
        	editor.commit();
        }
        else
        	highscore_loose = (score.getInt(GLOBAL_HIGHSCORE_LOOSE, 0));
    }
    
	@Override
	protected void onPause() {
		super.onPause();
		SharedPreferences.Editor editor;
    	editor = score.edit();
		editor.putInt(GLOBAL_HIGHSCORE_WIN, highscore_win);
		editor.commit();
		
		editor = score.edit();
		editor.putInt(GLOBAL_HIGHSCORE_LOOSE, highscore_loose);
		
		editor.commit();
	}
    
    private void newGame() {
    	Intent intent = new Intent(this, NewGameActivity.class); 
    	intent.putExtra(GAME_LOGIC,game);
    	
    	startActivityForResult(intent,GAME_LOGIC_RESULT); 
    }

    private void highScore() {
		Intent intent = new Intent(getBaseContext(), HighscoreActivity.class);
		intent.putExtra(GLOBAL_HIGHSCORE_WIN, highscore_win);
		intent.putExtra(GLOBAL_HIGHSCORE_LOOSE, highscore_loose);
		intent.putExtra(LOCAL_HIGHSCORE_WIN, game.getWin());
		intent.putExtra(LOCAL_HIGHSCORE_LOOSE, game.getLoose());
		startActivityForResult(intent,HIGHSCORE_RESULT); 
    }
    
    private void lang() {
    	Intent intent = new Intent(getBaseContext(), ChooseLangActivity.class);
    	startActivityForResult(intent, LANGUAGE_RESULT); 
    }
    
    private void about() {
    	Intent intent = new Intent(getBaseContext(), AboutActivity.class);
    	startActivityForResult(intent,0);	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.quit_application:
    		finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
    	super.onActivityResult(requestCode,resultCode,data); 
    	if(resultCode==QUIT)
    		finish(); 
    	
    	if(requestCode==GAME_LOGIC_RESULT){
    		if(resultCode==RESULT_OK){
    			game = data.getParcelableExtra(GAME_LOGIC);
    			if((Boolean) data.getBooleanExtra(NEW_GAME,false))
    				newGame();
    		}
    	}
    	else if(requestCode==LANGUAGE_RESULT){
    		if(resultCode==RESULT_OK){
    			Intent intent = getIntent();
    			finish();
    			startActivity(intent); 
    		}
    	}
    	else if(requestCode==HIGHSCORE_RESULT){
    		if(resultCode==RESULT_OK){
    			game.setWin(0);
    			game.setLoose(0);
    			SharedPreferences.Editor editor;
    			editor = score.edit();
    			editor.putInt(GLOBAL_HIGHSCORE_WIN, 0);
    			editor.putInt(GLOBAL_HIGHSCORE_LOOSE, 0); 
    			highscore_win = 0;
    			highscore_loose = 0; 
    			editor.commit();
    		}
    	}
    }
} // end of class MainActivity
