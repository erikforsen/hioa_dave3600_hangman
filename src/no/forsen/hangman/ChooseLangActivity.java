package no.forsen.hangman;

import no.forsen.hangmann.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ChooseLangActivity extends Activity implements OnClickListener{
	final String LANGUAGE = "Language";
	final String NOR = "nb";
	final String ENG = "en";
	
	@Override
	protected void onCreate(Bundle savedInstanceState ){
		super.onCreate(savedInstanceState);
		Language.loadLang(this); 
		setContentView(R.layout.activity_chooselang);
		
		final Button btn_nor = (Button)findViewById(R.id.btn_nor); 
		final Button btn_eng = (Button)findViewById(R.id.btn_eng);
		btn_nor.setTypeface(MainActivity.tf);
		btn_eng.setTypeface(MainActivity.tf); 
		btn_nor.setOnClickListener(this);
		btn_eng.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_nor:
			Language.changeLang(NOR,this);
			break;
		case R.id.btn_eng:
			Language.changeLang(ENG,this);
			break; 
		}
		Intent intent = getIntent();
		setResult(RESULT_OK,intent);
		finish(); 
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu){
    	getMenuInflater().inflate(R.menu.main, menu);
    	return true;
    }
    
	@Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.quit_application:
    		finish();
    		return true;
    	default:
    		return super.onOptionsItemSelected(item);
    	}
    }
} // end of class ChooseLangActivity
