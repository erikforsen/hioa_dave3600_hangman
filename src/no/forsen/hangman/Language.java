package no.forsen.hangman;

import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class Language {
	private final static String LANGUAGE = "language"; 
	private final static String DEFAULT_LANGUAGE = "en"; 
	public static void changeLang(String lang, Context con ){
		 Resources res = con.getResources();
		 DisplayMetrics dm = res.getDisplayMetrics();
		 android.content.res.Configuration conf = res.getConfiguration();
		 conf.locale = new Locale(lang);
		 res.updateConfiguration(conf, dm);
		 saveLang(lang,con); 
	}
	
	private static void saveLang(String lang, Context con){
		    SharedPreferences prefs = con.getSharedPreferences(LANGUAGE, Activity.MODE_PRIVATE);
		    SharedPreferences.Editor editor = prefs.edit();
		    editor.putString(LANGUAGE, lang);
		    editor.commit(); 
	}
	
	public static void loadLang(Context con){
		SharedPreferences prefs = con.getSharedPreferences(LANGUAGE, Activity.MODE_PRIVATE);
		String lang = prefs.getString(LANGUAGE, DEFAULT_LANGUAGE); 
		changeLang(lang,con);
	}
} // end of class Language